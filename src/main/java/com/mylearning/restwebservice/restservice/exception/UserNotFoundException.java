package com.mylearning.restwebservice.restservice.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.http.*;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5507161253023716354L;

	
	public UserNotFoundException(String message) {
		super(message);
		
	}
	
	

}
