package com.mylearning.restwebservice.restservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mylearning.restwebservice.restservice.bean.HelloWorldBean;

@RestController
public class HelloController {
	
	
	@GetMapping(path = "/hello-world")
	public String helloword() {
		return "<h1>first controller</h1>";
	}

	@GetMapping(path = "/hello-world-bean")
	public HelloWorldBean hellowordbean() {
		return new HelloWorldBean("Hello bean calling");
	}

	@GetMapping(path = "/hello-world/pathparameter/{name}")
	public HelloWorldBean hellowordPathParameter(@PathVariable String name) {
		return new HelloWorldBean(String.format("calling path variable, %s ", name) );
	}
	
}
