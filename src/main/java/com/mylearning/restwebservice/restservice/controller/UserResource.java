package com.mylearning.restwebservice.restservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mylearning.restwebservice.restservice.bean.User;
import com.mylearning.restwebservice.restservice.exception.UserNotFoundException;
import com.mylearning.restwebservice.restservice.service.UserDaoService;

@RestController
public class UserResource {

	@Autowired
	UserDaoService userDaoService;

	
	@GetMapping("/user/getAllUsers")
	public  List<User> retriveAllUser() {
		return userDaoService.getAllUser();
	}
	
	@GetMapping("/user/{id}")
	public  User retriveUserById(@PathVariable Integer id) {
		
		User user = userDaoService.getspecificUserById(id);
		
		if(user==null) {
			
			throw  new UserNotFoundException("id "+id);
		}
		
		
		return user;
	}
	
	@PostMapping("/add-user")
	public  User addNewUser(@Valid @RequestBody User user) {
		return userDaoService.saveUser(user);
	}
	
	//no call defination found (not able to load jar in classpath
	/*@PostMapping("/add-user/return-uri")
	public  ResponseEntity<Object> addUserWithReturnURI(@Valid @RequestBody User user) {
		User user1= userDaoService.saveUser(user);
		
		URI location=  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user1.getId()).toUri();
		return ResponseEntity.created(location).build();
	}*/
	
	
	
}
