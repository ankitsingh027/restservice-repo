package com.mylearning.restwebservice.restservice.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mylearning.restwebservice.restservice.bean.Post;
import com.mylearning.restwebservice.restservice.bean.User;
import com.mylearning.restwebservice.restservice.dao.UserRepository;
import com.mylearning.restwebservice.restservice.exception.UserNotFoundException;
import com.mylearning.restwebservice.restservice.service.UserDaoService;

@RestController
public class UserResourceJPA {

	@Autowired
	UserDaoService userDaoService;

	@Autowired
	UserRepository userRepository;
	
	@GetMapping("JPA/user/getAllUsers")
	public  List<User> retriveAllUser() {
		return userRepository.findAll();
	}
	
	@GetMapping("JPA/user/{id}")
	public  Optional<User> retriveUserById(@PathVariable Integer id) {
		
		Optional<User> user = userRepository.findById(id);
		
		if(!user.isPresent()) {
			
			throw  new UserNotFoundException("id "+id);
		}
		
		
		return user;
	}
	
	@DeleteMapping("JPA/user/{id}")
	public  void deleteUserById(@PathVariable Integer id) {
		
		userRepository.deleteById(id);
		
		
	}
	
	@PostMapping("JPA/add-user")
	public  User addNewUser(@Valid @RequestBody User user) {
		return userRepository.save(user);
	}
	
	
	@GetMapping("JPA/users/{id}/posts")
	public  List<Post> retrivePostsByUserId(@PathVariable Integer id) {
		
		Optional<User> user = userRepository.findById(id);
		
		if(!user.isPresent()) {
			
			throw  new UserNotFoundException("id "+id);
		}
		
		
		return user.get().getPostList();
	}
	
	//no call defination found (not able to load jar in classpath
	/*@PostMapping("/add-user/return-uri")
	public  ResponseEntity<Object> addUserWithReturnURI(@Valid @RequestBody User user) {
		User user1= userDaoService.saveUser(user);
		
		URI location=  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user1.getId()).toUri();
		return ResponseEntity.created(location).build();
	}*/
	
	
	
}
