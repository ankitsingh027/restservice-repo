package com.mylearning.restwebservice.restservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mylearning.restwebservice.restservice.bean.Name;
import com.mylearning.restwebservice.restservice.bean.PersonVersioning1;
import com.mylearning.restwebservice.restservice.bean.PersonVersioning2;

@RestController
public class VersionCheckController {
	
	
	/*
	 * @GetMapping("/getName/v1") public PersonVersioning1 getNameOfPersonNamev1() {
	 * 
	 * return new PersonVersioning1("Ankit"); }
	 * 
	 * @GetMapping("/getName/v2") public PersonVersioning2 getNameOfPersonNamev2() {
	 * 
	 * return new PersonVersioning2("Ankit","Singh"); }
	 */

	
	/****************Using Versioning using request param************/
	
	@GetMapping(value = "/person/param" , params ="version=1" )
	//@GetMapping(value = "/v1/person" )
	public PersonVersioning1 param1() {
		
		return new PersonVersioning1("Ankit Singh");
	}
	
	
	@GetMapping(value="/person/param", params ="version=2" )
	//@GetMapping(value = "/v2/person" )
	public PersonVersioning2 param2() {
		
		return new PersonVersioning2( new Name( "Ankit","Singh"));
	}
	
	/***************Using Header one*************/
	
	
	@GetMapping(value = "/person/header", headers = "X-API-VERSION=1")
	public PersonVersioning1 header1() {

		return new PersonVersioning1("Ankit Singh");
	}

	@GetMapping(value = "/person/header", headers = "X-API-VERSION=2")
	public PersonVersioning2 header2() {

		return new PersonVersioning2(new Name("Ankit", "Singh"));
	}
	
	
	/***************Using produces*************/
	
	@GetMapping(value = "/person/produces", produces = "application/lnd.company.app-v1+json")
	public PersonVersioning1 produce1() {

		return new PersonVersioning1("Ankit Singh");
	}

	@GetMapping(value = "/person/produces", produces = "application/lnd.company.app-v2+json")
	public PersonVersioning2 produce2() {

		return new PersonVersioning2(new Name("Ankit", "Singh"));
	}
}
