package com.mylearning.restwebservice.restservice.bean;

public class PersonVersioning2 {

	Name name;

	public PersonVersioning2(Name name) {
		super();
		this.name = name;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

}
