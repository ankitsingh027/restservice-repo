package com.mylearning.restwebservice.restservice.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

//@ApiModel("All User Details")
//@JsonIgnoreProperties(value = {"id"})

@Entity
public class User {

	// @JsonIgnore
	// if want to ignore the property
	@Id
	@GeneratedValue
	Integer id;

	@Size(min = 2, message = "name shoud have atleast 2 characters")
	String name;

	@Past
	// @ApiModelProperty("Birth Date should be past")
	Date dob;

	@OneToMany(mappedBy = "user")
	List<Post> postList;

	public User() {

	}

	public User(int id, String name, Date dob) {
		super();
		this.id = id;
		this.name = name;
		this.dob = dob;
	}

	public User(Integer id, @Size(min = 2, message = "name shoud have atleast 2 characters") String name,
			@Past Date dob, List<Post> postList) {
		super();
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.postList = postList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public List<Post> getPostList() {
		return postList;
	}

	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}

}
