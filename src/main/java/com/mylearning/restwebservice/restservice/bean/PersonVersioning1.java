package com.mylearning.restwebservice.restservice.bean;

public class PersonVersioning1 {

	String name;

	public PersonVersioning1(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
