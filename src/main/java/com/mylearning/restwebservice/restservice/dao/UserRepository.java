package com.mylearning.restwebservice.restservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mylearning.restwebservice.restservice.bean.User;

@Repository
public interface UserRepository extends JpaRepository<User , Integer> {

}
