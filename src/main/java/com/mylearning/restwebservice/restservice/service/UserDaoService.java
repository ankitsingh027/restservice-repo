package com.mylearning.restwebservice.restservice.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mylearning.restwebservice.restservice.bean.User;

@Component
public class UserDaoService {
	
	static List<User> list = new ArrayList<>();
	public static int count=4;
	static {
		
		
		list.add(new User(1,"Ankit",new Date()));
		list.add(new User(2,"bhim",new Date()));
		list.add(new User(3,"dhimu",new Date()));
		list.add(new User(4,"naru",new Date()));
	}

	public List<User> getAllUser(){
		
		return list;
	}
	
	public User saveUser(User user) {
		
		if(user.getId()==null) {
			
			list.add(new User(count++,user.getName(),user.getDob()==null?new Date():user.getDob()));
			
		}
		else {
			count=user.getId();
			list.add(new User(user.getId(),user.getName(),user.getDob()==null?new Date():user.getDob()));
			
		}
		
		return list.get(count-1);
		
	}
	
public User getspecificUserById(Integer id) {
		
		if(id !=null) {
			return list.stream().filter(e -> e.getId()==id).findFirst().orElse(null);
			
		}
		
		return null;
		
	}
}
